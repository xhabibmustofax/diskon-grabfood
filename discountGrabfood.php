<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Hitung Diskon Online Food</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="shortcut icon" href="https://dinus.ac.id/img/icon.ico" />

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-GTM5W7YT8J"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'G-GTM5W7YT8J');
		</script>
	</head>
	<body>
		<style>
		/* * { font-family:Arial; } */
		h2 { color: #224f99; }
		
		#htmltoimage {
            width: 40%;
            margin: auto;
			background-color: #fff;
        }
		</style>

	 	<div class="container-fluid h-100">
	 		<div class="text-center">
	    		<img src="https://dinus.ac.id/img/logo%20udinus%20official%20round.png" alt="" style="width: 10%;margin: 25px 0 20px;">
				<h2>Hitung Diskonan <span style="color:green;">Online Food</span></h2>
				<p style="color: #8e8e8e;margin-bottom: 30px;">Buat anda yang sering patungan tapi bingung membagi diskonan</p>
	    	</div>
		    <div class="row justify-content-center align-items-center h-100">
				<?php    
				if(isset($_POST['submit'])){ //check if form was submitted
				?>
				<div class="col-md-12 text-center">
					<div class="pt-3 pb-3 mb-3" id="htmltoimage">
						<div id="copyArea">
							<h2 class="mb-3">Hasil Tagihan</h2>
							<?php
							$person_price = $_POST['person'];
							$person_name = $_POST['person_name'];
							$discount = $_POST['disc'] - $_POST['ongkir'];
							function mixPerson($key, $val) {
								return array('key'=>$key,'val'=>$val);
							}
							$mixResult = array_map('mixPerson', $person_name, $person_price);

							$jumlahperson = count($person_price);
							$totalnominal = array_sum($person_price);
							$x = 0;
							foreach ($mixResult as $item) {
								if (isset($_POST['pajak'])) {
									$pajak = ($item['val']*10)/100;
								}else{
									$pajak = 0;
								}
								
								$persenan = ($item['val'] * 100) / $totalnominal;
								$jmldisc = ($discount * $persenan) / 100;
								$bayar = ($item['val'] - $jmldisc) + $pajak;
								if (!empty($bayar)) {
									$price = ceil($bayar);
								}
								
								echo '<h5>Tagihan <strong>'.ucfirst($item['key']).'</strong> <del class="text-muted">Rp. '.number_format($person_price[$x],0,',','.').'</del>, jadi bayar <span class="text-success">Rp. '.number_format($price,0,',','.').'</span></h5>';
								$x++;
							}
							?>
						</div>
					</div>
					<button class="btn btn-lg btn-outline-secondary" onclick="goBack()">Go Back</button>
					<button class="btn btn-lg btn-outline-primary" onclick="downloadimage()">Save as PNG</button>
					<button class="btn btn-lg btn-outline-primary btnCopy" onclick="copyDivToClipboard()" data-toggle="tooltip">Copy to Clipboard</button>

				</div>
				<?php
				}
				else{
				?>

				<div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3">
		            <form class="" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
						<div class="nm_tag">
							<div class="form-group">
								<input type="number" class="form-control form-control-lg rupiah" id="disc" name="disc" value="" placeholder="Jumlah Diskon" required autocomplete="off" />
							</div>
							<div class="form-group">
								</label><input type="number" class="form-control form-control-lg rupiah" id="ongkir" name="ongkir" value="" placeholder="Ongkir + Biaya Layanan" required autocomplete="off" />
							</div>
							<div class="form-group">
								<div class="form-check">
								    <input type="checkbox" name="pajak" class="form-check-input" id="pajak">
								    <label class="form-check-label" for="pajak">PPN 10%</label>
									<small class="text-muted">(Hanya pilih jika ada PPN 10%)</small>
							  	</div>
							 </div>
							<div class="form-row element" id='div_1'>
									<div class="col"><input type="text" class="form-control form-control-lg mb-3" name="person_name[]" value="" placeholder="Nama" required autocomplete="off" /></div>
									<div class="col"><input type="number" class="form-control form-control-lg rupiah" name="person[]" value="" placeholder="Tagihan" required autocomplete="off" /></div>
							</div>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary add">Tambah Orang</button>
							<button type="submit" name="submit" class="btn btn-success float-right">Hitung</button>
						</div>
					</form>
				<?php 
				} ?>
				</div>
		    </div>
	        <div class="text-center">
	        	<h6 style="margin-top: 50px;">made with <i class="fa fa-heart" aria-hidden="true" style="color:#e63946"></i> by <a target='_blank' href="https://instagram.com/mustbib">@mustbib</a></h6>
	        </div>
		</div>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.3.3/html2canvas.min.js" integrity="sha512-adgfzougYIGhG3Tpb47fZLuMwaULLJQdujqOeWFoGc7vwFvBrFkhaPkJPId5swgdr122mghL/ysQk4oiabmRCQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		
		<script>
			$(document).ready(function(){

				$(".add").click(function(){
					
					var lastid = $(".element:last").attr("id");
					var split_id = lastid.split("_");
					var nextindex = Number(split_id[1]) + 1;

					$(".element:last").after("<div class='form-row element' id='div_"+ nextindex +"'></div>");
					
					$("#div_" + nextindex).append('<div class="col-6"><label for="p_name"><input type="text" class="form-control form-control-lg mb-3" name="person_name[]" value="" placeholder="Nama" required autocomplete="off" /></label></div><div class="col-6"><label for="p_sects"><input type="number" class="form-control form-control-lg" id="p_sect" name="person[]" placeholder="Tagihan" autocomplete="off" /></label></div> <button type="button" style="position: absolute; right: -40px" class="btn btn-danger btn-lg remove" id="remove_' + nextindex + '"><i class="fa fa-times" aria-hidden="true"></i></button>');
				});

				$('.nm_tag').on('click','.remove',function(){
			
					var id = this.id;
					var split_id = id.split("_");
					var deleteindex = split_id[1];

					$("#div_" + deleteindex).remove();

				});
			});
		</script>

		<script>

			$('.btnCopy').tooltip({
				trigger: 'click',
				placement: 'right',
				title: 'Copied!'
			});
			$(document).ready( function () { 
				setInterval(function () {
					$('.btnCopy').tooltip('hide'); 
				}, 2300);
			});
		</script>

		<script type="text/javascript">
			function goBack() {
				window.history.back();
			}

			function downloadimage() {
				var utc = new Date().toLocaleDateString().slice(0,10).replace(/-/g,'/');
				var container = document.getElementById("htmltoimage"); //specific element on page
				html2canvas(container, { allowTaint: true }).then(function (canvas) {

					var link = document.createElement("a");
					document.body.appendChild(link);
					link.download = "bukti_online_food-"+utc;
					link.href = canvas.toDataURL();
					link.target = '_blank';
					link.click();
				});
			}

			function copyDivToClipboard() {
				var range = document.createRange();
				range.selectNode(document.getElementById("copyArea"));
				window.getSelection().removeAllRanges(); // clear current selection
				window.getSelection().addRange(range); // to select text
				document.execCommand("copy");
				window.getSelection().removeAllRanges();// to deselect
			}
		</script>

	</body>
</html>